<?php
/*
Plugin Name: FZS Directory Plugin
Plugin URI: http://the406.com
Description: A simple add-on to support world domination!
Version: 1.2.8
Author: Bradford Knowlton
Author URI: http://www.bradknowlton.com
Text Domain: fzs
Domain Path: /languages

------------------------------------------------------------------------
Copyright 2012-2017 Bradford Knowlton

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
*
* Changelog
* 
* 1.2.7 Initial Version
* 1.2.8 Added Custom Post Type
* 1.2.9 
*
**/

define( 'FZS_DIRECTORY_VERSION', '1.1.7' );

define( 'FZS_DIRECTORY_PATH', plugin_dir_path( __FILE__ ) );

class FZS_Directory{
	
	public function __construct()
    {
    	add_action( 'init', array( $this, 'register_directory_listing' ) );
    }
	
	function register_directory_listing() {
	    register_post_type( 'directory_listing',
	        array(
	            'labels' => array(
	                'name' => __('Directory Listings', 'fzs'),
	                'singular_name' => __('Directory Listing', 'fzs'),
	                'add_new' => __('Add New', 'fzs'),
	                'add_new_item' => __('Add New Directory Listing', 'fzs'),
	                'edit' => __('Edit', 'fzs'),
	                'edit_item' => __('Edit Directory Listing', 'fzs'),
	                'new_item' => __('New Directory Listing', 'fzs'),
	                'view' => __('View', 'fzs'),
	                'view_item' => __('View Directory Listing', 'fzs'),
	                'search_items' => __('Search Directory Listings', 'fzs'),
	                'not_found' => __('No Directory Listings found', 'fzs'),
	                'not_found_in_trash' => __('No Directory Listing found in trash', 'fzs'),
	                'parent' => __('Parent Directory Listing', 'fzs')
	            ),
	 
	            'public' => true,
	            'menu_position' => 15,
	            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
	            'taxonomies' => array( '' ),
	            'menu_icon' => 'dashicons-media-spreadsheet',
	            'has_archive' => true
	        )
	    );
	}
	
}

$fzs_directory = new FZS_Directory();


